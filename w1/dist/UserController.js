"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const routing_controllers_1 = require("routing-controllers");
let UserController = class UserController {
    //輸出名為UserController的函式
    getAll(request, response) {
        var _a;
        if (((_a = request.cookies) === null || _a === void 0 ? void 0 : _a.islogin) === 'yes') {
            return response.send('is login');
        }
        else {
            return response.send('not login');
        }
    }
    post(request, response) {
        if (request.body.account === 'abc' && request.body.password === '123') {
            response.cookie('islogin', 'yes');
            return response.json({ result: 'Cookie is Saving !!' });
        }
        return response.json({ result: 'Cookie is not Saving !!' });
    }
    getOne(request, response) {
        response.clearCookie('islogin');
        return response.send('logout');
    }
};
__decorate([
    (0, routing_controllers_1.Get)('/'),
    __param(0, (0, routing_controllers_1.Req)()),
    __param(1, (0, routing_controllers_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "getAll", null);
__decorate([
    (0, routing_controllers_1.Post)('/login'),
    __param(0, (0, routing_controllers_1.Req)()),
    __param(1, (0, routing_controllers_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "post", null);
__decorate([
    (0, routing_controllers_1.Get)('/logout'),
    __param(0, (0, routing_controllers_1.Req)()),
    __param(1, (0, routing_controllers_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "getOne", null);
UserController = __decorate([
    (0, routing_controllers_1.Controller)() //routing controller 不能直接使用response.send做輸出 需要加上return
], UserController);
exports.UserController = UserController;
