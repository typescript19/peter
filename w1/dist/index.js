"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const morgan_1 = __importDefault(require("morgan"));
const body_parser_1 = __importDefault(require("body-parser"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const routing_controllers_1 = require("routing-controllers");
require("reflect-metadata");
const UserController_1 = require("./UserController");
//以上皆為引入套件或是檔案
const app = (0, express_1.default)();
app.use((0, morgan_1.default)('dev'));
app.use(express_1.default.json());
dotenv_1.default.config();
app.use((0, cookie_parser_1.default)()); //使用cookie-parser
app.use(body_parser_1.default.json()); //使用body-parser
app.use(body_parser_1.default.urlencoded({
    extended: true,
}));
app.post('/', (request, response) => {
    response.send(request.body);
});
//routing-controllers 使用 UserController 檔案內的 controllers 去做判斷
//useExpressServer單獨創建和配置 express 應用程序
(0, routing_controllers_1.useExpressServer)(app, {
    controllers: [UserController_1.UserController],
});
//express使用auth內部建立的函式
// app.get('/', auth.index);
// app.post('/login', auth.login);
// app.get('/logout', auth.logout);
app.listen(process.env.PORT);
