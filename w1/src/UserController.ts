import { Request, Response } from 'express';
import { Controller, Param, Body, Get, Post, Req, Res, Put, Delete } from 'routing-controllers';

@Controller() //routing controller 不能直接使用response.send做輸出 需要加上return
export class UserController {
  //輸出名為UserController的函式
  @Get('/')
  getAll(@Req() request: Request, @Res() response: Response) {
    if (request.cookies?.islogin === 'yes') {
      return response.send('is login');
    } else {
      return response.send('not login');
    }
  }

  @Post('/login')
  post(@Req() request: Request, @Res() response: Response) {
    if (request.body.account === 'abc' && request.body.password === '123') {
      response.cookie('islogin', 'yes');
      return response.json({ result: 'Cookie is Saving !!' });
    }

    return response.json({ result: 'Cookie is not Saving !!' });
  }

  @Get('/logout')
  getOne(@Req() request: Request, @Res() response: Response) {
    response.clearCookie('islogin');
    return response.send('logout');
  }
}
